# Raw Data

Raw data extractable from `Game.Units.ShipController`.

`ShipController.Ship.Hull.GetAllComponents()` returns a list of all `HullComponent`

`HullComponent.PositionInHull` contains `Vector3` data on position inside the hull

`HullComponent.CurrentHealth` contains `float` representing current health points
`HullComponent.MaxHealth` contains `float` representing maximum health points
`HullComponent.IsFunctional` contains `bool` representing if the component is still functional
`HullComponent.DebuffCount` contains `int` representing number of debuffs
`HullComponent.IsOnFire()` returns `bool` representing if components are on fire
