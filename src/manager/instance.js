const fs = require('fs-extra');
const path = require('path');
const child_process = require('child_process');
const ship = require('./ship');
const config = require('../common/config');
const utils = require('../common/utils');

module.exports = class GameInstance {
  constructor(onRestart) {
    this.id = Date.now().toFixed(0);
    this.gameDir = path.resolve(`./tmp/gamedir_${this.id}`);
    this.testStartDate = 0;
    this.keepRunning = true;
    this.test = {};
    this.testNumber = 0;
    this.version = '';
    this.gameProcess = null;
    this.timeoutChecker = null;
    this.onRestart = onRestart;
    this.copyGameDir();
  }
  copyGameDir() {
    try { fs.mkdirSync(this.gameDir); } catch(err) {}
    fs.copySync(path.resolve('./tmp/gamedir'), this.gameDir);
    try { fs.rmSync(`${this.gameDir}/Mods`, { recursive: true, force: true }); } catch(err) {}
    try { fs.rmSync(`${this.gameDir}/Saves`, { recursive: true, force: true }); } catch(err) {}
  }
  removeGameDir() {
    try { fs.rmSync(this.gameDir, { recursive: true, force: true }); } catch(err) {}
  }
  startProcess() {
    this.onRestart(this);
    let args = [
      '-screen-fullscreen', '0',
      '-screen-height', config.manager.screenHeight,
      '-screen-width', config.manager.screenWidth,
      '-tester-id', this.id,
    ];
    if(config.manager.headless) {
      args.splice(0,0,'-batchmode');
    }
    this.gameProcess = child_process.spawn('Nebulous.exe', args, { cwd: this.gameDir });
    this.gameProcess.on('close', this.onCrash.bind(this));
  }
  onNewTest(test, testNumber) {
    this.test = test;
    this.testNumber = testNumber;
    this.testStartDate = Date.now();
    try { clearTimeout(this.timeoutChecker); } catch(err) {}
    let attackingShip = test.setup.attackingShip.ship;
    let attackingShipXml = ship.generateShip('Attacker', attackingShip.hullType, attackingShip.mounts, attackingShip.compartments, attackingShip.modules);
    let targetShip = test.setup.targetShip.ship;
    let targetShipXml = ship.generateShip('Target', targetShip.hullType, targetShip.mounts, targetShip.compartments, targetShip.modules);
    try { fs.mkdirSync(path.resolve(`${this.gameDir}/Saves`)); } catch(err) {}
    try { fs.mkdirSync(path.resolve(`${this.gameDir}/Saves/Fleets`)); } catch(err) {}
    fs.writeFileSync(path.resolve(`${this.gameDir}/Saves/Fleets/Attacker.fleet`), attackingShipXml);
    fs.writeFileSync(path.resolve(`${this.gameDir}/Saves/Fleets/Target.fleet`), targetShipXml);
    if(!config.manager.disableRestart) {
      this.timeoutChecker = setTimeout(() => {
        console.info(`[Game Instance ${this.id}] Test did not finish, restarting game process`);
        this.gameProcess.kill('SIGKILL');
      }, utils.maxTime(test, config.common.loadTime));
    }
  }
  onVersionInfo(version) {
    this.version = version;
  }
  onCrash() {
    if(this.keepRunning) {
      console.info(`[Game Instance ${this.id}] Detected game process closed, starting new game process`);
      this.startProcess();
    }
  }
  kill() {
    this.keepRunning = false;
    this.gameProcess.kill('SIGKILL');
  }
}