const express = require('express');
const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');
const config = require('../common/config');
const result = require('./result');
const GameInstance = require('./instance');

const app = express();
const port = 9999;

//Checking for existence of game directory
try { fs.mkdirSync(path.resolve('./tmp/gamedir')); } catch(err) {}
if(!fs.existsSync(path.resolve('./tmp/gamedir/Nebulous.exe'))) {
  console.info('[Game Manager] Game not found in tmp/gamedir, quitting...');
  process.exit();
}
try { fs.rmSync(path.resolve('./tmp/gamedir/Mods'), { recursive: true, force: true }); } catch(err) {}
try { fs.rmSync(path.resolve('./tmp/gamedir/Saves'), { recursive: true, force: true }); } catch(err) {}

//Removing previous tmp game directories
let previousGameDirList = fs.readdirSync(path.resolve('./tmp/')).filter(e => e.includes('gamedir'));
for(let previousGameDir of previousGameDirList) {
  if(previousGameDir != 'gamedir') {
    try { fs.rmSync(path.resolve(`./tmp/${previousGameDir}`), { recursive: true, force: true }); } catch(err) {}
  }
}

//Preemptive test request
const newTest = async (gameInstance) => {
  //Get test from server
  let serverResponse = await fetch(`${config.manager.serverUrl}/test`);
  let data = await serverResponse.json();
  let currTest = data.test;
  let testNumber = data.testNumber;
  if(currTest.timeScale > config.manager.maxTimeScale) {
    currTest.timeScale = config.manager.maxTimeScale;
  }
  //Send test info to instance to generate ship xml
  gameInstance.onNewTest(currTest, testNumber);
}
//Callback for when game instance is restarted
const gameInstanceRestart = async (gameInstance) => {
  try {
    await newTest(gameInstance);
  }
  catch(err) {
    if(err.code === 'ECONNREFUSED') {
      process.exit();
    }
    else {
      gameInstance.kill();
    }
  }
}

//Start game instances
let gameInstances = [];
for(let i = 0;i < config.manager.maxInstances;i++) {
  gameInstances.push(new GameInstance(gameInstanceRestart));
}
for(gameInstance of gameInstances) {
  gameInstance.startProcess();
}

//Periodically restart if game instances got killed
//Also clean up any gamedirs still lying around
setInterval(() => {
  for(let i = 0;i < gameInstances.length;i++) {
    let gameInstance = gameInstances[i];
    if(!gameInstance.keepRunning) {
      gameInstance.removeGameDir();
      gameInstances[i] = new GameInstance(gameInstanceRestart);
      gameInstances[i].startProcess();
    }
  }
}, 5*60*1000);

app.use(express.json({ limit: '1tb' }));
app.get('/test/:testerId', async (req, res) => {
  console.info(`[Game Manager] Received test info request`);
  //Send test info to game process
  for(gameInstance of gameInstances) {
    if(gameInstance.id === req.params.testerId) {
      res.send(gameInstance.test);
    }
  }
});
app.put('/version/:testerId', (req, res) => {
  let data = req.body;
  console.info(`[Game Manager] Received game version`);
  //Send version info to instance to store
  for(gameInstance of gameInstances) {
    if(gameInstance.id === req.params.testerId) {
      gameInstance.onVersionInfo(data.Version);
    }
  }
  res.end();
});
app.put('/result/:testerId', async (req, res) => {
  let data = req.body;
  console.info(`[Game Manager] Received test result`);
  for(gameInstance of gameInstances) {
    if(gameInstance.id === req.params.testerId) {
      //Sending test results to server
      let currResult = {
        name: gameInstance.test.name,
        testNumber: gameInstance.testNumber,
        test: gameInstance.test,
        version: gameInstance.version,
        eventLog: data
      };
      currResult = result.filterResult(currResult);
      await fetch(`${config.manager.serverUrl}/result`, {
        method: 'put',
        body: JSON.stringify(currResult),
        headers: {'Content-Type': 'application/json'}
      });
      try {
        await newTest(gameInstance);
      }
      catch(err) {
        gameInstance.kill();
      }
    }
  }
  res.end();
});
app.listen(port, () => {
  console.info(`[Game Manager] NEBFLTCOM Automated Testing Game Manager is listening on port ${port}`);
});

process.on('exit', () => {
  for(gameInstance of gameInstances) {
    gameInstance.kill();
  }
});