exports.formatName = (name) => {
  return name.replace(/ /g, '_').replace(/['".\[\]\-()]/g, '').replace(/_+/g, '_').toLowerCase();
}

exports.maxTime = (test, loadTime) => {
  return (loadTime * 1000) + (test.maxTime * (1000 / test.timeScale));
}