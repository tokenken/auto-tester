using BepInEx;
using HarmonyLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

namespace plugin {
  class Patch {
    [HarmonyPatch(typeof(Bundles.BundleManager), "ProcessAssetBundle")]
    [HarmonyPostfix]
    static void ProcessAssetBundlePostfix() {
      var _components = (Dictionary<string, Ships.HullComponent>)AccessTools.Field(typeof(Bundles.BundleManager), "_components").GetValue(Bundles.BundleManager.Instance);

      foreach(var _component in _components) {
        var component = _component.Value;
        if(
          component.ComponentName == "CR10 Antenna" ||
          component.ComponentName == "Plant Control Center" ||
          component.ComponentName == "Supplementary Radio Amplifiers"
        ) {
          AccessTools.Field(typeof(Ships.HullComponent), "_maxHealth").SetValue(component, 10000f);
          AccessTools.Field(typeof(Ships.HullComponent), "_damageThreshold").SetValue(component, 0f);
        }
      }
    }
    [HarmonyPatch(typeof(Game.TimeDilationManager), "SyncTargetTimescale")]
    [HarmonyPrefix]
    static bool SyncTargetTimescalePrefix(ref Game.TimeDilationManager __instance) {
      return false;
    }
    [HarmonyPatch(typeof(Game.Units.ShipController), "UpdateStatusSummary")]
    [HarmonyPrefix]
    static bool UpdateStatusSummaryPrefix(ref Game.Units.ShipController __instance) {
      return false;
    }
    [HarmonyPatch(typeof(Game.Units.ShipController), "HasOffensiveAbility")]
    [HarmonyPostfix]
    static void HasOffensiveAbilityPostfix(ref bool __result) {
      __result = true;
    }
    [HarmonyPatch(typeof(Ships.RezzingMuzzle), "Fire", new Type[] { typeof(Vector3) })]
    [HarmonyPrefix]
    static bool RezzingMuzzleFirePrefix(ref Ships.RezzingMuzzle __instance, Vector3 shotDirection) {
      var _ammoSource = (Munitions.IMagazine)AccessTools.Property(typeof(Ships.RezzingMuzzle), "_ammoSource").GetValue(__instance);
      var _shipController = (Game.Units.ShipController)AccessTools.Property(typeof(Ships.RezzingMuzzle), "_ship").GetValue(__instance);
      var _weapon = (Ships.WeaponComponent)AccessTools.Property(typeof(Ships.RezzingMuzzle), "_weapon").GetValue(__instance);
      var data = new FireBulletEvent();
      data.EventName = "FireBulletEvent";
      data.Time = Time.time;
      data.OriginShip = _shipController.Ship.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)_weapon);
      data.MunitionName = _ammoSource.AmmoType.MunitionName;
      data.FireDirection = new Vector3Data();
      data.FireDirection.x = shotDirection.x;
      data.FireDirection.y = shotDirection.y;
      data.FireDirection.z = shotDirection.z;
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.ContinuousWeaponComponent), "StartFiring")]
    [HarmonyPrefix]
    static bool ContinuousWeaponStartFiringPrefx(ref Ships.ContinuousWeaponComponent __instance) {
      var data = new FireContinuousEvent();
      data.EventName = "FireContinuousEvent";
      data.Time = Time.time;
      data.OriginShip = __instance.Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData(__instance);
      var ignoreRange = false;
      var shotDirection = AccessTools.Method(typeof(Ships.WeaponComponent), "GetAimPoint").Invoke(__instance, new object[] { ignoreRange });
      if(shotDirection != null) {
        data.FireDirection = new Vector3Data();
        data.FireDirection.x = ((Vector3)shotDirection).x;
        data.FireDirection.y = ((Vector3)shotDirection).y;
        data.FireDirection.z = ((Vector3)shotDirection).z;
      }
      data.IsStart = true;
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.ContinuousWeaponComponent), "StopFiring")]
    [HarmonyPrefix]
    static bool ContinuousWeaponStopFiringPrefx(ref Ships.ContinuousWeaponComponent __instance) {
      var data = new FireContinuousEvent();
      data.EventName = "FireContinuousEvent";
      data.Time = Time.time;
      data.OriginShip = __instance.Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData(__instance);
      var ignoreRange = false;
      var shotDirection = AccessTools.Method(typeof(Ships.WeaponComponent), "GetAimPoint").Invoke(__instance, new object[] { ignoreRange });
      if(shotDirection != null) {
        data.FireDirection = new Vector3Data();
        data.FireDirection.x = ((Vector3)shotDirection).x;
        data.FireDirection.y = ((Vector3)shotDirection).y;
        data.FireDirection.z = ((Vector3)shotDirection).z;
      }
      data.IsStart = false;
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.BaseCellLauncherComponent), nameof(Ships.Controls.IWeapon.FirePosition))]
    [HarmonyPrefix]
    static bool CellMissileFirePositionPrefix(ref Ships.BaseCellLauncherComponent __instance, Vector3 position) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetPosition = new Vector3Data();
      data.TargetPosition.x = position.x * 10;
      data.TargetPosition.y = position.y * 10;
      data.TargetPosition.z = position.z * 10;
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.BaseCellLauncherComponent), nameof(Ships.Controls.IWeapon.FireTrack))]
    [HarmonyPrefix]
    static bool CellMissileFireTrackPrefix(ref Ships.BaseCellLauncherComponent __instance, Game.Sensors.ITrack track) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetTrack = Recorder.ExtractTrackData(track);
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.BaseCellLauncherComponent), nameof(Ships.Controls.IWeapon.FirePath))]
    [HarmonyPrefix]
    static bool CellMissileFirePathPrefix(ref Ships.BaseCellLauncherComponent __instance, List<Vector3> path) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetPath = new Vector3Data[path.Count];
      for(int i = 0;i < path.Count;i++) {
        var currTargetPath = new Vector3Data();
        currTargetPath.x = path[i].x * 10;
        currTargetPath.y = path[i].y * 10;
        currTargetPath.z = path[i].z * 10;
        data.TargetPath[i] = currTargetPath;
      }
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.TubeLauncherComponent), nameof(Ships.Controls.IWeapon.FirePosition))]
    [HarmonyPrefix]
    static bool TubeMissileFirePositionPrefix(ref Ships.TubeLauncherComponent __instance, Vector3 position) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetPosition = new Vector3Data();
      data.TargetPosition.x = position.x * 10;
      data.TargetPosition.y = position.y * 10;
      data.TargetPosition.z = position.z * 10;
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.TubeLauncherComponent), nameof(Ships.Controls.IWeapon.FireTrack))]
    [HarmonyPrefix]
    static bool TubeMissileFireTrackPrefix(ref Ships.TubeLauncherComponent __instance, Game.Sensors.ITrack track) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetTrack = Recorder.ExtractTrackData(track);
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Ships.TubeLauncherComponent), nameof(Ships.Controls.IWeapon.FirePath))]
    [HarmonyPrefix]
    static bool TubeMissileFirePathPrefix(ref Ships.TubeLauncherComponent __instance, List<Vector3> path) {
      var data = new FireMissileEvent();
      data.EventName = "FireMissileEvent";
      data.Time = Time.time;
      data.OriginShip = ((Ships.HullComponent)__instance).Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData((Ships.HullComponent)__instance);
      data.MunitionName = __instance.SelectedAmmoType.MunitionName;
      data.TargetPath = new Vector3Data[path.Count];
      for(int i = 0;i < path.Count;i++) {
        var currTargetPath = new Vector3Data();
        currTargetPath.x = path[i].x * 10;
        currTargetPath.y = path[i].y * 10;
        currTargetPath.z = path[i].z * 10;
        data.TargetPath[i] = currTargetPath;
      }
      EventBuffer.Instance.AddData(data);
      return true;
    }
    [HarmonyPatch(typeof(Game.Units.ShipController), "ApplyArmorDamage")]
    [HarmonyPostfix]
    static void ArmorDamagePostfix(ref Game.Units.ShipController __instance, Munitions.MunitionHitInfo hitInfo, ref float damageDone, ref bool ricochet, bool __result) {
      var data = new ArmorDamageEvent();
      data.EventName = "ArmorDamageEvent";
      data.Time = Time.time;
      data.OriginShip = __instance.Ship.ShipNameNoPrefix;
      data.MunitionHit = Recorder.ExtractMunitionHitData(hitInfo);
      data.DidRicochet = ricochet;
      data.DamageDone = damageDone;
      EventBuffer.Instance.AddData(data);
    }
    [HarmonyPatch(typeof(Ships.HullComponent), "DoDamage")]
    [HarmonyPostfix]
    static void DamagePostfix(ref Ships.HullComponent __instance, ref float damageDone, Munitions.MunitionHitInfo hitInfo) {
      var data = new ComponentDamageEvent();
      data.EventName = "ComponentDamageEvent";
      data.Time = Time.time;
      data.OriginShip = __instance.Socket.MyHull.MyShip.ShipNameNoPrefix;
      data.Component = Recorder.ExtractComponentData(__instance);
      data.MunitionHit = Recorder.ExtractMunitionHitData(hitInfo);
      data.DamageDone = damageDone;
      EventBuffer.Instance.AddData(data);
    }
    [HarmonyPatch(typeof(Munitions.Missile), "TerminateMissile")]
    [HarmonyPostfix]
    static void TerminateMissilePostfix(ref Munitions.Missile __instance, Munitions.ActiveMissileSalvo.MissileStatus status) {
      if(status == Munitions.ActiveMissileSalvo.MissileStatus.Hardkilled || status == Munitions.ActiveMissileSalvo.MissileStatus.Softkilled ||status == Munitions.ActiveMissileSalvo.MissileStatus.Hit) {
        var data = new DestroyMissileEvent();
        data.EventName = "DestroyMissileEvent";
        if(status == Munitions.ActiveMissileSalvo.MissileStatus.Hit) {
          data.EventName = "MissileHitEvent";
        }
        data.Time = Time.time;
        var _launchedFrom = (Game.Units.ShipController)AccessTools.Property(typeof(Munitions.Missile), "_launchedFrom").GetValue(__instance);
        data.OriginShip = _launchedFrom.Ship.ShipNameNoPrefix;
        data.MunitionName = __instance.MunitionName;
        data.MissilePosition = new Vector3Data();
        data.MissilePosition.x = __instance.transform.position.x * 10;
        data.MissilePosition.y = __instance.transform.position.y * 10;
        data.MissilePosition.z = __instance.transform.position.z * 10;
        EventBuffer.Instance.AddData(data);
      }
    }
  }
}