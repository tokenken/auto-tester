using HarmonyLib;
using UnityEngine;
using Utility;

namespace plugin {
  class SettingsOverride {
    public static void Init() {
      var instance = GameSettings.Instance;
      QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
      QualitySettings.antiAliasing = 0;
      QualitySettings.lodBias = 0.001f;
      QualitySettings.masterTextureLimit = 10;
      QualitySettings.shadows = ShadowQuality.Disable;
      QualitySettings.softParticles = false;
      Application.targetFrameRate = 20;
    }
    [HarmonyPatch(typeof(Utility.PlayerPreferencesWrapper), "RetrieveFloat")]
    [HarmonyPostfix]
    static void RetrieveFloatPostfix(string key, float defaultValue, ref float __result) {
      Debug.Log($"Retrieve Float using key: \"{key}\" value: {__result}");
      if(key == "vol_master") {
        __result = -80;
      }
    }
    [HarmonyPatch(typeof(Utility.PlayerPreferencesWrapper), "RetrieveInt")]
    [HarmonyPostfix]
    static void RetrieveIntPostfix(string key, int defaultValue, ref int __result) {
      Debug.Log($"Retrieve Int using key: \"{key}\" value: {__result}");
      if(key == "quality_ShadowQual") {
        __result = 0;
      }
      if(key == "quality_AnisoFiltering") {
        __result = 0;
      }
      if(key == "quality_AntiAliasing") {
        __result = 0;
      }
    }
    [HarmonyPatch(typeof(Utility.PlayerPreferencesWrapper), "RetrieveBool")]
    [HarmonyPostfix]
    static void RetrieveBoolPostfix(string key, bool defaultValue, ref bool __result) {
      Debug.Log($"Retrieve Bool using key: \"{key}\" value: {__result}");
    }
    [HarmonyPatch(typeof(Utility.PlayerPreferencesWrapper), "RetrieveString")]
    [HarmonyPostfix]
    static void RetrieveStringPostfix(string key, string defaultValue, ref string __result) {
      Debug.Log($"Retrieve String using key: \"{key}\" value: {__result}");
    }
  }
}