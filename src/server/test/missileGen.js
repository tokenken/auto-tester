module.exports = (bodies = ['SGM-2 Body'], maneuvers = ['Weave', 'Corkscrew', 'None'], warheads = ['HE Impact']) => {
  let missiles = [];
  let engineConfigs = [
    { A: 1, B: 0, C: 0 },
    { A: 0, B: 1, C: 0 },
    { A: 0, B: 0, C: 1 },
    { A: 0.5, B: 0.5, C: 0 },
    { A: 0.5, B: 0, C: 0.5 },
    { A: 0, B: 0.5, C: 0.5 },
    { A: 0.33, B: 0.33, C: 0.33 },
  ];
  bodies.forEach(body => {
    maneuvers.forEach(maneuver => {
      engineConfigs.forEach(engine => {
        warheads.forEach(warhead => {
          let currKey = 's';
          let size = 0;
          if(body.includes('1')) {
            size = 5;
            currKey += '1';
          }
          else if(body.includes('2')) {
            size = 5;
            currKey += '2';
          }
          else if(body.includes('3')) {
            size = 7;
            currKey += '3';
          }
          currKey += maneuver[0].toLowerCase();
          currKey += `a${Math.round(engine.A*3)}b${Math.round(engine.B*3)}c${Math.round(engine.C*3)}`;
          if(warhead.includes('Impact')) {
            currKey += 'imp';
          }
          else if(body.includes('Blast ER')) {
            currKey += 'ber';
          }
          else if(body.includes('Blast')) {
            currKey += 'bla';
          }
          let currMissile = {
            prefix: body.replace(' Body', ''),
            name: 'Missile',
            body: body,
            components: [
              {
                size: 1,
                type: 'CommandSeeker',
                name: 'Command Receiver',
                settings: {
                  Mode: 'Targeting'
                }
              },
              {
                size: 1
              },
              {
                size: 1,
                type: 'DirectGuidance',
                name: 'Adv. Direct Guidance',
                settings: {
                  Role: 'Offensive',
                  HotLaunch: false,
                  SelfDestructOnLost: false,
                  Maneuvers: maneuver,
                  DefensiveDoctrine: {
                    TargetSizeMask: 12,
                    TargetSizeOrdering: 'Descending',
                    FarthestFirst: false
                  },
                  ApproachAngleControl: true
                }
              },
              {
                size: size,
                type: null,
                name: warhead
              },
              {
                size: size,
                type: 'MissileEngine',
                name: null,
                settings: {
                  BalanceValues: engine
                }
              }
            ]
          }
          missiles.push({
            key: currKey,
            missile: currMissile,
            range: 100000,
            custom: {
              body: body,
              maneuvers: maneuvers,
              engine: engine
            }
          });
        });
      });
    });
  });
  return missiles;
}