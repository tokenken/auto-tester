const deepcopy = require('deepcopy');
const baseTest = require('../base');

let baseAttacker = {
  hullType: 'solomon',
  mounts: [
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    {
      componentName: 'VLS-2 Launcher',
      componentData: [
        {
          munitionName: 'SGM-222 Hurricane',
          quantity: 16
        }
      ]
    },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'RF101 \'Bullseye\' Radar' }
  ],
  compartments: [
    { componentName: 'Basic CIC' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' }
  ],
  modules: [
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'RS41 \'Spyglass\' Radar' }
  ]
};

let baseTarget = {
  hullType: 'solomon',
  mounts: [
    { componentName: 'Mk20 \'Defender\' PDT' },
    { componentName: 'Mk20 \'Defender\' PDT' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'Mk25 \'Rebound\' PDT' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'Mk25 \'Rebound\' PDT' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'RF101 \'Bullseye\' Radar' }
  ],
  compartments: [
    { componentName: 'Reinforced CIC' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 },
        { munitionName: '15mm Sandshot', quantity: 5000 }
      ]
    },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Reinforced CIC' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 },
        { munitionName: '15mm Sandshot', quantity: 5000 }
      ]
    },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 },
        { munitionName: '15mm Sandshot', quantity: 5000 }
      ]
    },
    { componentName: 'Reinforced CIC' },
    { componentName: 'Reinforced CIC' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 },
        { munitionName: '15mm Sandshot', quantity: 5000 }
      ]
    },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 },
        { munitionName: '15mm Sandshot', quantity: 5000 }
      ]
    }
  ],
  modules: [
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FM500 Drive' },
    { componentName: 'FM500 Drive' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'RM50 \'Parallax\' Radar' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' }
  ]
};

exports.generateTests = () => {
  let tests = [];
  let currTest = deepcopy(baseTest);
  currTest.name = `Validation PDT Test`;
  currTest.type = `Validation PDT Test`;
  currTest.testAmount = 1000;
  currTest.maxTime = 60;
  currTest.timeScale = 50;
  currTest.description = `Test designed for comparing different testing setups and validating test environments.`;
  //Create attacker
  let currAttacker = deepcopy(baseAttacker);
  currTest.setup.attackingShip.ship = currAttacker;
  currTest.setup.attackingShip.position.x = 7500;
  //Create target
  let currTarget = deepcopy(baseTarget);
  currTest.setup.targetShip.ship = currTarget;
  tests.push(currTest);
  return tests;
}