const deepcopy = require('deepcopy');
const dict = require('../../common/dict');
const config = require('../../common/config');
const baseTest = require('./base');

let ranges = [];
for(let r = 0;r < 12;r++) {
  ranges.push((r + 1) * 1000);
}

let orientations = [
  {
    name: 'Front',
    x: 0,
    y: 90,
    z: 0
  },
  {
    name: 'Back',
    x: 0,
    y: -90,
    z: 0
  },
  {
    name: 'Right',
    x: 0,
    y: 0,
    z: 0
  },
  {
    name: 'Top',
    x: 0,
    y: 0,
    z: -90
  },
  {
    name: 'Bottom',
    x: 0,
    y: 0,
    z: 90
  }
];

let radarTypes = {
  parallax: 'RM50 \'Parallax\' Radar',
  frontline: 'RS35 \'Frontline\' Radar',
  spyglass: 'RS41 \'Spyglass\' Radar'
};
let radarList = Object.keys(radarTypes);
let radarMaxRange = {
  parallax: 9500,
  frontline: 8000,
  spyglass: 11500
};

let weaponTypes = {
  ap120: '120mm AP Shell',
  he120: '120mm HE Shell',
  rpf120: '120mm HE-RPF Shell',
  ap250: '250mm AP Shell',
  he250: '250mm HE Shell',
  rpf250: '250mm HE-RPF Shell',
  ap300: '300mm AP Rail Sabot',
  ap450: '450mm AP Shell',
  he450: '450mm HE Shell',
  beam: 'Particle Beam'
};
let weaponList = Object.keys(weaponTypes);
let weaponMountTypes = {
  ap120: 'Mk62 Cannon',
  he120: 'Mk62 Cannon',
  rpf120: 'Mk62 Cannon',
  ap250: 'Mk64 Cannon',
  he250: 'Mk64 Cannon',
  rpf250: 'Mk64 Cannon',
  ap300: 'Mk82 Railgun',
  ap450: 'Mk68 Cannon',
  he450: 'Mk68 Cannon',
  beam: 'Mk610 Beam Turret'
};
let weaponMaxRange = {
  ap120: 7200,
  he120: 7200,
  rpf120: 7200,
  ap250: 8000,
  he250: 8000,
  rpf250: 8000,
  ap300: 24000,
  ap450: 11250,
  he450: 11250,
  beam: 5000
};
let weaponAvailableMounts = {
  ap120: [1,2,3,4,5,6,7],
  he120: [1,2,3,4,5,6,7],
  rpf120: [1,2,3,4,5,6,7],
  ap250: [1,2,3,4],
  he250: [1,2,3,4],
  rpf250: [1,2,3,4],
  ap300: [1,3,4],
  ap450: [1,3,4],
  he450: [1,3,4],
  beam: [1,3,4]
};
let weaponMaxAmmo = {
  ap120: 14400,
  he120: 14400,
  rpf120: 14400,
  ap250: 4799,
  he250: 4799,
  rpf250: 4799,
  ap300: 960,
  ap450: 900,
  he450: 900,
  beam: 0
};

let baseAttacker = {
  hullType: 'solomon',
  mounts: [
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' }
  ],
  compartments: [
    { componentName: 'Basic CIC' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' }
  ],
  modules: [
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'Energy Regulator' },
    { componentName: 'Energy Regulator' },
    { componentName: 'Energy Regulator' },
    { componentName: 'Energy Regulator' },
    { componentName: 'Energy Regulator' },
    { componentName: 'Ammunition Elevators' },
    { componentName: 'Ammunition Elevators' },
    { componentName: 'Ammunition Elevators' },
    { componentName: 'Ammunition Elevators' },
    { componentName: 'Ammunition Elevators' },
    { componentName: 'RS35 \'Frontline\' Radar' }
  ]
};

let hullTypes = [
  'sprinter',
  'raines',
  'keystone',
  'vauxhall',
  'axford',
  'solomon'
];
let baseTarget = {
  sprinter: {
    mounts: dict.hullTypeSocketKeys.sprinter.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.sprinter.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.sprinter.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  },
  raines: {
    mounts: dict.hullTypeSocketKeys.raines.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.raines.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.raines.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  },
  keystone: {
    mounts: dict.hullTypeSocketKeys.keystone.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.keystone.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.keystone.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  },
  vauxhall: {
    mounts: dict.hullTypeSocketKeys.vauxhall.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.vauxhall.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.vauxhall.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  },
  axford: {
    mounts: dict.hullTypeSocketKeys.axford.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.axford.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.axford.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  },
  solomon: {
    mounts: dict.hullTypeSocketKeys.solomon.mounts.map(e => {
      return { componentName: 'CR10 Antenna' };
    }),
    compartments: dict.hullTypeSocketKeys.solomon.compartments.map(e => {
      return { componentName: 'Plant Control Center' };
    }),
    modules: dict.hullTypeSocketKeys.solomon.modules.map(e => {
      return { componentName: 'Supplementary Radio Amplifiers' };
    })
  }
};

exports.generateTests = () => {
  let tests = [];
  if(config.server.gunTests.damageTests) {
    orientations.forEach(orientation => {
      hullTypes.forEach(hullType => {
        weaponList.forEach(weapon => {
          radarList.forEach(radar => {
            ['None','RF101 \'Bullseye\' Radar'].forEach(fcr => {
              let maxWeaponRange = weaponMaxRange[weapon];
              let maxRadarRange = radarMaxRange[radar];
              let maxFCRRange = fcr.includes('Bullseye') ? 9000 : 12000;
              if(radar !== 'spyglass' && fcr.includes('Bullseye')) {
                maxFCRRange = 0;
              }
              ranges.filter(r => (r < maxWeaponRange && r < maxRadarRange && r < maxFCRRange)).forEach(range => {
                let currTest = deepcopy(baseTest);
                currTest.name = `${orientation.name} side of ${hullType[0].toUpperCase() + hullType.substring(1)}`;
                currTest.name += ` vs ${weaponTypes[weapon]}`;
                currTest.name += ` using`;
                if(radar === 'parallax') {
                  currTest.name += ` Parallax`;
                }
                if(radar === 'frontline') {
                  currTest.name += ` Frontline`;
                }
                if(radar === 'spyglass') {
                  currTest.name += ` Spyglass`;
                }
                if(fcr.includes('Bullseye')) {
                  currTest.name += ` with Bullseye`;
                }
                currTest.name += ` at ${range}m`;
                currTest.type = `Weapon Damage Test`;
                currTest.custom = {
                  orientation: orientation.name,
                  hull: hullType,
                  weapon: weapon,
                  radar: radar,
                  fcr: fcr.includes('Bullseye'),
                  range: range
                };
                currTest.testAmount = 1;
                currTest.maxTime = 300;
                currTest.description = `Test designed for studying the hit groups and accuracy of different gun based weapon systems.`;
                //Create attacker
                let currAttacker = deepcopy(baseAttacker);
                //Create weapon mounts
                for(let weaponMount of weaponAvailableMounts[weapon]) {
                  currAttacker.mounts[weaponMount - 1] = { componentName: weaponMountTypes[weapon] };
                }
                //Add Ammo
                let ammoAmount = weaponMaxAmmo[weapon];
                if(ammoAmount > 0) {
                  let bulkMag = {
                    componentName: 'Bulk Magazine',
                    componentData: [{ munitionName: weaponTypes[weapon], quantity: ammoAmount }]
                  };
                  currAttacker.compartments[1] = bulkMag;
                  currAttacker.compartments[2] = bulkMag;
                  currAttacker.compartments[8] = bulkMag;
                  currAttacker.compartments[11] = bulkMag;
                  currAttacker.compartments[12] = bulkMag;
                  currAttacker.compartments[13] = bulkMag;
                  currAttacker.compartments[14] = bulkMag;
                  currAttacker.compartments[15] = bulkMag;
                }
                //Add FCR
                currAttacker.mounts[12] = { componentName: fcr };
                //Change Radar
                currAttacker.modules[14] = { componentName: radarTypes[radar] };
                currTest.setup.attackingShip.ship = currAttacker;
                currTest.setup.attackingShip.position.x = range;
                //Create target
                let currTarget = deepcopy(baseTarget[hullType]);
                currTarget.hullType = hullType;
                currTest.setup.targetShip.ship = currTarget;
                currTest.setup.targetShip.rotation.rx = orientation.x;
                currTest.setup.targetShip.rotation.ry = orientation.y;
                currTest.setup.targetShip.rotation.rz = orientation.z;
                tests.push(currTest);
              });
            });
          });
        });
      });
    });
  }
  return tests;
}