const deepcopy = require('deepcopy');
const combinatorics = require('js-combinatorics');
const config = require('../../common/config');
const baseTest = require('./base');
const missileGen = require('./missileGen');

let ranges = [3000, 9000];

let pdtMountGroups = [
  [1],
  [2],
  [6,10]
];

let radarTypes = {
  parallax: 'RM50 \'Parallax\' Radar'
};
let radarList = Object.keys(radarTypes);
let radarMaxRange = {
  parallax: 9500,
  frontline: 8000,
  spyglass: 11500
};

let pdtTypes = {
  defender: '20mm Slug',
  rebound: 'Flak Round',
  stonewall: 'Flak Round',
  aurora: 'Particle Beam',
  sarissa: '15mm Sandshot'
};
let pdtList = Object.keys(pdtTypes);
let pdtMountTypes = {
  defender: 'Mk20 \'Defender\' PDT',
  rebound: 'Mk25 \'Rebound\' PDT',
  stonewall: 'Mk29 \'Stonewall\' PDT',
  aurora: 'Mk90 \'Aurora\' PDT',
  sarissa: 'Mk95 \'Sarissa\' PDT'
};
let pdtPermutations = [...new combinatorics.BaseN(pdtList, pdtMountGroups.length)].map(e => e.sort()).sort();
let pdtCombinations = [];
let pdtCombinationKeys = [];
for(let pdtPermutation of pdtPermutations) {
  if(!pdtCombinationKeys.includes(pdtPermutation.join())) {
    pdtCombinationKeys.push(pdtPermutation.join());
    pdtCombinations.push(pdtPermutation);
  }
}

let missiles = missileGen();

let weaponTypes = {};
let weaponMountTypes = {};
let weaponMaxRange = {};
let weaponCustom = {};
let weaponAvailableMounts = {};
let weaponMaxAmmo = {};
for(let i = 0;i < missiles.length;i++) {
  let currMissile = missiles[i].missile;
  let currMissileKey = missiles[i].key;
  let currMissileRange = missiles[i].range;
  let currMissileCustom = missiles[i].custom;
  weaponTypes[currMissileKey] = currMissile;
  weaponMaxRange[currMissileKey] = currMissileRange;
  if(currMissileKey.includes('s1')) {
    weaponMountTypes[currMissileKey] = 'VLS-1-46 Launcher';
    weaponAvailableMounts[currMissileKey] = [1,2,3,4,5,6,7];
    weaponMaxAmmo[currMissileKey] = 46;
  }
  if(currMissileKey.includes('s2')) {
    weaponMountTypes[currMissileKey] = 'VLS-2 Launcher';
    weaponAvailableMounts[currMissileKey] = [1,2,3,4,5,6,7];
    weaponMaxAmmo[currMissileKey] = 16;
  }
  if(currMissileKey.includes('s3')) {
    weaponMountTypes[currMissileKey] = 'VLS-3 Launcher';
    weaponAvailableMounts[currMissileKey] = [1,2,3,4];
    weaponMaxAmmo[currMissileKey] = 16;
  }
  weaponCustom[currMissileKey] = currMissileCustom;
}
let weaponList = Object.keys(weaponTypes);

let baseAttacker = {
  hullType: 'solomon',
  mounts: [
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'None' },
    { componentName: 'RF101 \'Bullseye\' Radar' }
  ],
  compartments: [
    { componentName: 'Basic CIC' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' },
    { componentName: 'Bulk Magazine' }
  ],
  modules: [
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus Array' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'Missile Programming Bus' },
    { componentName: 'RS41 \'Spyglass\' Radar' }
  ]
};

let baseTarget = {
  hullType: 'solomon',
  mounts: [],
  compartments: [
    { componentName: 'Reinforced CIC' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 }
      ]
    },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    { componentName: 'Reinforced CIC' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 }
      ]
    },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 }
      ]
    },
    { componentName: 'Reinforced CIC' },
    { componentName: 'Reinforced CIC' },
    { componentName: 'Berthing' },
    { componentName: 'Berthing' },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 }
      ]
    },
    {
      componentName: 'Reinforced Magazine',
      componentData: [
        { munitionName: '20mm Slug', quantity: 25000 },
        { munitionName: 'Flak Round', quantity: 5000 }
      ]
    }
  ],
  modules: [
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FR4800 Reactor' },
    { componentName: 'FM500 Drive' },
    { componentName: 'FM500 Drive' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'RS35 \'Frontline\' Radar' },
    { componentName: 'FR3300 Micro Reactor' },
    { componentName: 'FR3300 Micro Reactor' }
  ]
};

exports.generateTests = () => {
  let tests = [];
  if(config.server.pdtTests.gunTests) {
    pdtCombinations.filter(c => c.filter(p => p !== 'none').length > 0).forEach(pdtCombination => {
      weaponList.forEach(weapon => {
        radarList.forEach(radar => {
          [0,1,2].forEach(jammerCount => {
            let maxWeaponRange = weaponMaxRange[weapon];
            let maxJammerRange = jammerCount > 0 ? 10000 : 20000;
            ranges.filter(r => (r < maxWeaponRange && r < maxRadarRange && r < maxJammerRange)).forEach(range => {
              let currTest = deepcopy(baseTest);
              let pdtCount = {};
              currTest.name = `${radar[0].toUpperCase() + radar.substring(1)} with`;
              pdtList.filter(e => e !== 'none').forEach(e => {
                let count = pdtCombination.filter(e2 => e2 === e).length;
                if(count > 0) {
                  currTest.name += ` ${count}x ${e[0].toUpperCase() + e.substring(1,3)}`;
                }
                pdtCount[e] = count;
              });
              let weaponCount = weaponAvailableMounts[weapon].length * weaponMaxAmmo[weapon];
              currTest.name += ` vs ${weaponCount}x ${weapon} Missiles`;
              if(jammerCount > 0) {
                currTest.name += ` with ${jammerCount}x Jammers`;
              }
              currTest.name += ` at ${range}m`;
              currTest.type = `PDT Gun Test`;
              currTest.custom = {
                pdtCount: pdtCount,
                weapon: weaponCustom[weapon],
                weaponName: weapon,
                radar: radar,
                jammerCount: jammerCount,
                range: range
              };
              currTest.testAmount = 3;
              currTest.maxTime = 210;
              currTest.description = `Test designed for studying the effectiveness of different gun PDT combinations versus different missile combinations.`;
              //Create attacker
              let currAttacker = deepcopy(baseAttacker);
              for(let weaponMount of weaponAvailableMounts[weapon]) {
                currAttacker.mounts[weaponMount - 1] = {
                  componentName: weaponMountTypes[weapon],
                  componentData: [
                    {
                      munitionName: weaponTypes[weapon].prefix + ' ' + weaponTypes[weapon].name,
                      quantity: weaponMaxAmmo[weapon]
                    }
                  ]
                };
              }
              if(jammerCount >= 1) {
                currAttacker.mounts[11] = { componentName: 'E90 \'Blanket\' Jammer' };
              }
              if(jammerCount >= 2) {
                currAttacker.mounts[12] = { componentName: 'E90 \'Blanket\' Jammer' };
              }
              currAttacker.missiles = [weaponTypes[weapon]];
              currTest.setup.attackingShip.ship = currAttacker;
              currTest.setup.attackingShip.position.x = range;
              //Create target
              let currTarget = deepcopy(baseTarget);
              for(let k = 0;k < pdtCombination.length;k++) {
                for(let j = 0;j < pdtMountGroups[k].length;j++) {
                  let currMount = pdtMountTypes[pdtCombination[k]];
                  currTarget.mounts[pdtMountGroups[k][j] - 1] = {
                    componentName: currMount
                  };
                }
              }
              for(let k = 0;k < currTarget.mounts.length;k++) {
                if(typeof currTarget.mounts[k] === 'undefined') {
                  currTarget.mounts[k] = { componentName: 'None' };
                }
              }
              currTest.setup.targetShip.ship = currTarget;
              currTest.setup.targetShip.orders.push({
                type: 'BattleShort',
                orderBoolData1: true
              });
              tests.push(currTest);
            });
          });
        });
      });
    });
  }
  return tests;
}