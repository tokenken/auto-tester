const path = require('path');
const fs = require('fs');

exports.generateTests = () => {
  let testList = fs.readdirSync(path.resolve('./tmp/customtests/')).filter(e => e.includes('.json'));
  let tests = [];
  for(let test of testList) {
    let testData = JSON.parse(fs.readFileSync(path.resolve(`./tmp/customtests/${test}`)));
    tests.push(testData);
  }
  return tests;
}