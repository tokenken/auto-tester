const getTimestamp = () => {
  return Date.now();
}

const getMonitoringData = async () => {
  let response = await fetch(`${window.location.origin}/monitor`);
  let data = await response.json();
  return data;
}

const displayStatistics = (monitoringData) => {
  let completionDateDisplay = document.getElementById('completionDateDisplay');
  let avgRateMinuteDisplay = document.getElementById('avgRateMinuteDisplay');
  let avgRateHourDisplay = document.getElementById('avgRateHourDisplay');
  completionDateDisplay.innerText = 'N/A';
  avgRateMinuteDisplay.innerText = 'N/A';
  avgRateHourDisplay.innerText = 'N/A';
  let hourDate = Date.now() - (1000*60*60);
  let minuteDate = Date.now() - (1000*60*10);
  let testTicketsDone = monitoringData.testTicketsDone;
  let hourCount = 0;
  let minuteCount = 0;
  for(let i = 0;i < testTicketsDone.length;i++) {
    let currTestTicket = testTicketsDone[i];
    if(currTestTicket.endDate > hourDate) {
      hourCount++;
    }
    if(currTestTicket.endDate > minuteDate) {
      minuteCount++;
    }
  }
  let totalTime = 60;
  if(testTicketsDone.length > 0) {
    totalTime = ((Date.now() - testTicketsDone[0].endDate) / 1000) / 60;
    let avgMinuteRate = minuteCount / (10 < totalTime ? 10 : totalTime);
    let avgHourRate = hourCount / (60 < totalTime ? 60 : totalTime);
    avgRateMinuteDisplay.innerText = `${(avgMinuteRate).toFixed(2)}`;
    avgRateHourDisplay.innerText = `${(avgHourRate).toFixed(2)}`;
    let testsLeft = monitoringData.testTicketsInProgress.length + monitoringData.testTicketsQueued.length;
    let timeLeft = (testsLeft / avgMinuteRate) * 60 * 1000;
    let completionDate = luxon.DateTime.fromMillis(Date.now() + timeLeft);
    completionDateDisplay.innerText = `${completionDate.toFormat('f')} (${completionDate.toRelative()})`;
  }
}

const countChartData = [
  {
    label: 'Finished Tests',
    backgroundColor: 'rgb(75, 192, 192)',
    borderColor: 'rgb(75, 192, 192)',
    cubicInterpolationMode: 'monotone',
    pointRadius: 0,
    pointHitRadius: 150,
    fill: true,
    data: []
  },
  {
    label: 'In Progress Tests',
    backgroundColor: 'rgb(255, 159, 64)',
    borderColor: 'rgb(255, 159, 64)',
    cubicInterpolationMode: 'monotone',
    pointRadius: 0,
    pointHitRadius: 150,
    fill: true,
    data: []
  },
  {
    label: 'Queued Tests',
    backgroundColor: 'rgb(255, 99, 132)',
    borderColor: 'rgb(255, 99, 132)',
    cubicInterpolationMode: 'monotone',
    pointRadius: 0,
    pointHitRadius: 150,
    fill: true,
    data: []
  }
];
const countChart = new Chart('queueChart', {
  type: 'line',
  data: {
    labels: [],
    datasets: countChartData
  },
  options: {
    animation: {
      easing: 'linear',
      duration: 900
    },
    animations: {
      y: {
        duration: 0,
        easing: 'linear',
        from: 1,
        to: 1
      }
    },
    scales: {
      x: {
        type: 'time',
        time: {
          tooltipFormat: 'f',
          minUnit: 'second'
        },
        title: {
          display: true,
          text: 'Date'
        },
        ticks: {
          autoSkipPadding: 10,
          maxRotation: 0
        }
      },
      y: {
        position: 'right',
        min: 0,
        suggestedMax: 100,
        stacked: true
      }
    }
  }
});
const updateCountChart = (monitoringData) => {
  if(countChart.data.labels.length > 3600) {
    countChart.data.labels.splice(0,1);
    countChart.data.datasets[0].data.splice(0,1);
    countChart.data.datasets[1].data.splice(0,1);
    countChart.data.datasets[2].data.splice(0,1);
  }
  countChart.data.labels.push(Date.now());
  countChart.data.datasets[0].data.push(monitoringData.testTicketsDone.length);
  countChart.data.datasets[1].data.push(monitoringData.testTicketsInProgress.length);
  countChart.data.datasets[2].data.push(monitoringData.testTicketsQueued.length);
  countChart.update();
}

//Start loop to grab monitoring data
let lastUpdate = Date.now();
const updateAll = async () => {
  if(Date.now() - lastUpdate > 3600 * 1000) {
    window.location.reload();
  }
  let monitoringData = await getMonitoringData();
  updateCountChart(monitoringData);
  displayStatistics(monitoringData);
  lastUpdate = Date.now();
}
window.setInterval(updateAll.bind(this), 10000);
updateAll();