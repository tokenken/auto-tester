const validateShip = (testShip, eventShip, shipName) => {
  if(eventShip.EventName !== 'ShipDataEvent') {
    return true;
  }
  if(eventShip.OriginShip !== shipName) {
    return true;
  }
  if(eventShip.Time > 0.5) {
    return true;
  }
  let testComponents = [];
  for(let mount of testShip.ship.mounts) {
    if(mount.componentName !== 'None') {
      testComponents.push(mount.componentName);
    }
  }
  for(let compartment of testShip.ship.compartments) {
    if(compartment.componentName !== 'None') {
      testComponents.push(compartment.componentName);
    }
  }
  for(let module of testShip.ship.modules) {
    if(module.componentName !== 'None') {
      testComponents.push(module.componentName);
    }
  }
  let eventComponents = [];
  for(let component of eventShip.Components) {
    eventComponents.push(component.ComponentName);
  }
  testComponents.sort();
  eventComponents.sort();
  if(testComponents.length !== eventComponents.length) {
    return false;
  }
  for(let i = 0;i < testComponents.length;i++) {
    if(testComponents[i] !== eventComponents[i]) {
      return false;
    }
  }
  return (
    Math.round(testShip.position.x / 10) === Math.round(eventShip.Position.x / 10) &&
    Math.round(testShip.position.y / 10) === Math.round(eventShip.Position.y / 10) &&
    Math.round(testShip.position.z / 10) === Math.round(eventShip.Position.z / 10)
  );
}

exports.validateResult = (result) => {
  for(let event of result.eventLog) {
    if(!validateShip(result.test.setup.attackingShip, event, 'Attacker')) {
      return false;
    }
    if(!validateShip(result.test.setup.targetShip, event, 'Target')) {
      return false;
    }
  }
  return true;
}
