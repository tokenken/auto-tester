const LZString = require('lz-string');

exports.generateHtml = (chartCode, allData) => `<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tabulator-tables@5.1.8/dist/css/tabulator.min.css" integrity="sha256-lgsoEQZ/1r5kOqnZPytNBbiUm9b6aHx7OrtnjrAygzg=" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/plotly.js/2.5.0/plotly.min.js" integrity="sha512-jMCLF90/C4/J/TNZtuowZn0erDL+1jpzdAklxIDs0O0Wr4qAHPt999DdN9WUNdYWpjfSOmLdBkltbP8Jki8mTg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/tabulator-tables@5.1.8/dist/js/tabulator.min.js" integrity="sha256-uF1FP9iFPcL1rLqsFPN21xfpQxspx8BxFJSQmYnjCU4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sylvester/0.1.3/sylvester.min.js" integrity="sha512-5LqT6fHFrg0Ig5mLH37WNbbdhxpwZbk0q0jzQaKlzy8Tg2w0HRvPpS8OXPs4r85Yd6uGP5vcXrViwzs/OVS/9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.min.js" integrity="sha512-qoCTmFwBtCPvFhA+WAqatSOrghwpDhFHxwAGh+cppWonXbHA09nG1z5zi4/NGnp8dUhXiVrzA6EnKgJA+fyrpw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div id="chartDiv"></div>
<script>
  let allData = JSON.parse(LZString.decompressFromBase64('${this.generateCompressedData(allData)}'));
  let targetElement = document.getElementById('chartDiv');
  ${chartCode}
</script>
</html>`;

exports.generateWiki = (chartCode, versions) => `[[html]]
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tabulator-tables@5.1.8/dist/css/tabulator.min.css" integrity="sha256-lgsoEQZ/1r5kOqnZPytNBbiUm9b6aHx7OrtnjrAygzg=" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/plotly.js/2.5.0/plotly.min.js" integrity="sha512-jMCLF90/C4/J/TNZtuowZn0erDL+1jpzdAklxIDs0O0Wr4qAHPt999DdN9WUNdYWpjfSOmLdBkltbP8Jki8mTg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/tabulator-tables@5.1.8/dist/js/tabulator.min.js" integrity="sha256-uF1FP9iFPcL1rLqsFPN21xfpQxspx8BxFJSQmYnjCU4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstat/1.9.5/jstat.min.js" integrity="sha512-MGT8BGoc8L3124PwHEGTC+M8Hu9oIbZOg8ENcd92sQKKidWKOOOZ6bqQemqYAX0yXJUnovOkF4Hx9gc/5lVxPw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sylvester/0.1.3/sylvester.min.js" integrity="sha512-5LqT6fHFrg0Ig5mLH37WNbbdhxpwZbk0q0jzQaKlzy8Tg2w0HRvPpS8OXPs4r85Yd6uGP5vcXrViwzs/OVS/9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.min.js" integrity="sha512-qoCTmFwBtCPvFhA+WAqatSOrghwpDhFHxwAGh+cppWonXbHA09nG1z5zi4/NGnp8dUhXiVrzA6EnKgJA+fyrpw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<div id="chartDiv">Loading...</div>
<script>
  fetch('INSERT_FILE_URL_HERE').then(async (res) => {
    let allData = JSON.parse(LZString.decompressFromBase64(await res.text()));
    let targetElement = document.getElementById('chartDiv');
    targetElement.innerText = '';
    ${chartCode}
  });
</script>
[[/html]]
[[div style="width: 100%; text-align: center; padding: 3px; border: 1px solid grey; background-color: #F6F9F6; padding-bottom: 0px; margin-bottom: 10px;"]]
//Data extracted from game version(s): ${versions.reduce((a, c) => a + ', ' + c)}//
[[/div]]
`;

exports.generateCompressedData = (allData) => LZString.compressToBase64(JSON.stringify(allData));
