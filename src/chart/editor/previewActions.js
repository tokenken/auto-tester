const serverUrl = window.location.origin;

window.currPreviewChartName = null;

const previewChartSelect = document.getElementById('preview-chart-select');
const previewChartSelectPlaceholder = 'Select Chart...';
const previewChartIframe = document.getElementById('previewIframe');
const previewRefreshButton = document.getElementById('preview-refresh-button');
const previewRunButton = document.getElementById('preview-run-button');

let resize = () => {
  let top = previewChartIframe.getBoundingClientRect().top;
  let height = window.innerHeight - top - 6;
  previewChartIframe.style.height = `${height}px`;
};
window.addEventListener('resize', resize);
document.getElementById('preview-tab').addEventListener('click', () => {
  window.setTimeout(() => {
    resize();
  }, 500);
});
resize();

window.previewLoadChartListAction = async () => {
  let res = await fetch(`${serverUrl}/list`);
  let data = await res.json();
  previewChartSelect.innerHTML = `<option ${window.currPreviewChartName === null ? 'selected' : ''}>${previewChartSelectPlaceholder}</option>`;
  data.forEach((chartName) => {
    previewChartSelect.innerHTML += `
    <option ${window.currPreviewChartName === chartName ? 'selected' : ''} value="${chartName}">${chartName}</option`;
  });
}
window.previewLoadChartListAction();

previewRefreshButton.addEventListener('click', () => {
  window.previewLoadChartListAction();
});

window.previewLoadChartAction = async (chartName) => {
  let res = await fetch(`${serverUrl}/get`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: chartName })
  });
  let data = await res.json();
  window.currPreviewChartName = chartName;
  previewRunButton.className ='btn btn-primary';
}
previewChartSelect.addEventListener('change', (event) => {
  let selectedChart = event.target.value;
  if(selectedChart !== previewChartSelectPlaceholder) {
    window.previewLoadChartAction(selectedChart);
  }
});

window.previewRunChartAction = async () => {
  previewChartIframe.innerHTML = `<div class="container-fluid position-relative">
  <div class="d-flex justify-content-center">
    <div class="spinner-border" role="status">
      <span class="visually-hidden">Loading...</span>
    </div>
  </div>
  <div class="d-flex justify-content-center">
    <figure class="text-center p-3" id="previewStatus"></figure>
  </div>
</div>`;
  let res = await fetch(`${serverUrl}/run`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: window.currPreviewChartName })
  });
  let data = await res.json();
  let html = data.html;
  previewChartIframe.innerHTML = '';
  let iframe = document.createElement('iframe');
  iframe.style.width = '100%';
  iframe.style.height = '100%';
  iframe.srcdoc = html;
  previewChartIframe.appendChild(iframe);
}
previewRunButton.addEventListener('click', () => {
  window.previewRunChartAction();
});

window.previewDisplayStatus = async () => {
  let previewStatusElement = document.getElementById('previewStatus');
  if(previewStatusElement !== null) {
    let res = await fetch(`${serverUrl}/status`);
    let data = await res.json();
    if(typeof data.finished !== 'undefined') {
      previewStatusElement.innerHTML = `<p class="h5">${data.finished} data points out of ${data.total} filtered</p>
<p class="h5">${(data.filterRate).toFixed(2)} filtered data points per second</p>
<p class="h6">Predicted to finish ${data.finishDateFromNow}</p>
<p><small><em>${data.finishDateFormat}</em></small></p>`;
    }
    else {
      previewStatusElement.innerHTML = '';
    }
  }
}
window.setInterval(() => {
  window.previewDisplayStatus();
}, 1000)