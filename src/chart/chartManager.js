const fs = require('fs');
const path = require('path');
const utils = require('../common/utils');

exports.dataDir = path.resolve('./tmp/data');
exports.chartDir = path.join(__dirname, '/charts');
exports.chartOutputDir = path.resolve('./tmp/charts');
exports.getChartNames = () => {
  let chartNames = [];
  let chartList = fs.readdirSync(this.chartDir).filter(e => e.includes('.json'));
  let tmpChartList = fs.readdirSync(this.chartDir).filter(e => e.includes('.tmp'));
  for(let chartPath of chartList) {
    let chart = JSON.parse(fs.readFileSync(path.resolve(`${this.chartDir}/${chartPath}`)));
    if(!chartNames.includes(chart.name)) {
      chartNames.push(chart.name);
    }
  }
  for(let chartPath of tmpChartList) {
    let chart = JSON.parse(fs.readFileSync(path.resolve(`${this.chartDir}/${chartPath}`)));
    if(!chartNames.includes(chart.name)) {
      chartNames.push(chart.name);
    }
  }
  return chartNames;
}

exports.getChart = (chartName) => {
  let formattedName = utils.formatName(chartName).substring(0, 100);
  if(fs.existsSync(path.resolve(`${this.chartDir}/${formattedName}.tmp`))) {
    let chart = JSON.parse(fs.readFileSync(path.resolve(`${this.chartDir}/${formattedName}.tmp`)));
    chart.tmp = true;
    return chart;
  }
  else if(fs.existsSync(path.resolve(`${this.chartDir}/${formattedName}.json`))) {
    let chart = JSON.parse(fs.readFileSync(path.resolve(`${this.chartDir}/${formattedName}.json`)));
    chart.tmp = false;
    return chart;
  }
  return null;
}

exports.newChart = (chartName) => {
  if(this.getChart(chartName) !== null) {
    return null;
  }
  let formattedName = utils.formatName(chartName).substring(0, 100);
  let newChart = {
    name: chartName,
    filterCode: 'return false;',
    transformCode: 'return allData;',
    chartCode: ''
  };
  fs.writeFileSync(path.resolve(`${this.chartDir}/${formattedName}.tmp`), JSON.stringify(newChart, null, 2));
  return this.getChart(chartName);
}

exports.editChart = (chart) => {
  let formattedName = utils.formatName(chart.name).substring(0, 100);
  delete chart.tmp;
  fs.writeFileSync(path.resolve(`${this.chartDir}/${formattedName}.tmp`), JSON.stringify(chart, null, 2));
  return this.getChart(chart.name);
}

exports.saveChart = (chartName) => {
  let chart = this.getChart(chartName);
  if(chart === null || !chart.tmp) {
    return null;
  }
  let formattedName = utils.formatName(chart.name).substring(0, 100);
  delete chart.tmp;
  fs.writeFileSync(path.resolve(`${this.chartDir}/${formattedName}.json`), JSON.stringify(chart, null, 2));
  fs.rmSync(path.resolve(`${this.chartDir}/${formattedName}.tmp`));
  return this.getChart(chart.name);
}
