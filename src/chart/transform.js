const fs = require('fs');
const path = require('path');
const md5 = require('md5');
const { jStat } = require('jstat');
const fetch = require('sync-fetch');
const chartManager = require('./chartManager');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

let transformFuncs = {};

function* createGenerator(dataPaths) {
  for(let dataPath of dataPaths) {
    yield JSON.parse(fs.readFileSync(path.resolve(`${chartManager.dataDir}/${dataPath}`)));
  }
}

exports.transformData = (chart, dataList) => {
  let chartTransformFuncCode = md5(chart.transformCode);
  if(typeof transformFuncs[chartTransformFuncCode] !== 'function') {
    transformFuncs[chartTransformFuncCode] = new Function('allData', 'jStat', 'fetch', chart.transformCode);
  }
  let transformFunc = transformFuncs[chartTransformFuncCode];
  let dataPaths = dataList.filter(d => d.name === chart.name).map(d => d.path);
  if(dataPaths.length > 0) {
    return transformFunc(createGenerator(dataPaths), jStat, fetch);
  }
  return null;
}