const fs = require('fs');
const path = require('path');
const chartManager = require('./chartManager');
const filter = require('./filter');
const transform = require('./transform');
const generator = require('./generator');
const utils = require('../common/utils');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

(async () => {
  //Get all chart code
  let charts = [];
  let chartNameList = chartManager.getChartNames();
  console.log(`[Chart] Grabbing all charts`);
  for(let chartName of chartNameList) {
    charts.push(chartManager.getChart(chartName));
  }

  //Filter data
  let filteredDataList = await filter.filterDataList(charts, (status) => {
    console.info(`[Chart] ${status.finished} data points out of ${status.total} filtered`);
    console.info(`[Chart] ${(status.filterRate).toFixed(2)} filtered data points per second`);
    console.info(`[Chart] Predicted to finish ${status.finishDate.fromNow()} (${status.finishDate.format('dddd, MMMM Do YYYY, h:mm:ss a')})`);
  });

  //Generate charts
  for(let chart of charts) {
    let currFilterDataList = filteredDataList.filter(f => f.name === chart.name);
    if(currFilterDataList.length > 0) {
      console.info(`[Chart] Generating chart: ${chart.name}`);
      console.info(`[Chart] Running transform code`);
      let transformedData = transform.transformData(chart, currFilterDataList);
      let formattedName = utils.formatName(chart.name).substring(0, 100);
      let wikiPath = path.resolve(`${chartManager.chartOutputDir}/${formattedName}.wiki.txt`);
      let htmlPath = path.resolve(`${chartManager.chartOutputDir}/${formattedName}.html`);
      let dataPath = path.resolve(`${chartManager.chartOutputDir}/${formattedName}.data.txt`);
      let dataJsonPath = path.resolve(`${chartManager.chartOutputDir}/${formattedName}.json`);
      console.info(`[Chart] Creating chart with chart code`);
      fs.writeFileSync(wikiPath, generator.generateWiki(chart.chartCode, currFilterDataList.map(f => f.versions).flat()));
      fs.writeFileSync(htmlPath, generator.generateHtml(chart.chartCode, transformedData));
      fs.writeFileSync(dataPath, generator.generateCompressedData(transformedData));
      fs.writeFileSync(dataJsonPath, JSON.stringify(transformedData));
    }
  }
});