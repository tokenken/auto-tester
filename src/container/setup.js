const child_process = require('child_process');

(async () => {
  console.info(`[Docker Setup] Building container`);
  let buildProcess = child_process.spawnSync('docker.exe', ['build', '.', '-f', './src/container/Dockerfile', '-t', 'nfc-at:latest']);
  let buildOutput = buildProcess.stdout.toString();
  console.info(buildOutput);
})();
