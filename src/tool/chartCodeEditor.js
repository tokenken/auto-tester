import TSWorker from 'url:monaco-editor/esm/vs/language/typescript/ts.worker.js';
import * as monaco from 'monaco-editor/esm/vs/editor/editor.main.js';

const filePond = require('filepond');
const fileDownload = require('js-file-download');
const utils = require('../common/utils');

self.MonacoEnvironment = {
  getWorkerUrl: () => { return TSWorker; }
};

let rootElement = document.getElementById('codeEditorContent');
rootElement.innerHTML += `<div>
  <h3>Load Code File</h3>
  <div class="mb-2">
    <div id="codeEditorContent-filePond"></div>
  </div>
  <div class="mb-2">
    <button id="codeEditorContent-loadCodeButton" type="button" class="btn btn-primary">Load</button>
  </div>
  <h3>Chart Name</h3>
  <div class="mb-2">
    <label for="codeEditorContent-codeNameInput" class="form-label">Name</label>
    <input type="text" class="form-control" id="codeEditorContent-codeNameInput" aria-describedby="codeEditorContent-codeNameDescription">
    <div id="codeEditorContent-codeNameDescription" class="form-text">Text used to uniquely identify the chart, which should be short as possible.</div>
  </div>
  <h3>Filter Code</h3>
  <div class="mb-2 border">
    <div id="codeEditorContent-filterCodeMirror" style="width: 100%; height: 20vh; border: 1px solid #ccc"></div>
  </div>
  <h3>Transform Code</h3>
  <div class="mb-2 border">
    <div id="codeEditorContent-transformCodeMirror" style="width: 100%; height: 80vh; border: 1px solid #ccc"></div>
  </div>
  <h3>Chart Code</h3>
  <div class="mb-2 border">
    <div id="codeEditorContent-chartCodeMirror" style="width: 100%; height: 80vh; border: 1px solid #ccc"></div>
  </div>
  <div class="mb-2">
    <button id="codeEditorContent-downloadCodeButton" type="button" class="btn btn-primary">Download Code</button>
  </div>
</div>`;

let chartCode = {
  name: '',
  filterCode: '',
  transformCode: '',
  chartCode: ''
};
const codeEditorFilePond = filePond.create({
  allowMultiple: false
});
document.getElementById('codeEditorContent-filePond').appendChild(codeEditorFilePond.element);
document.getElementById('codeEditorContent-loadCodeButton').onclick = async () => {
  let file = codeEditorFilePond.getFile();
  chartCode = JSON.parse(await file.file.text());
  document.getElementById(`codeEditorContent-codeNameInput`).value = chartCode.name;
  filterCodeMirror.setValue(chartCode.filterCode);
  transformCodeMirror.setValue(chartCode.transformCode);
  chartCodeMirror.setValue(chartCode.chartCode);
  codeEditorFilePond.removeFiles();
};

let filterCodeMirror = null;
let transformCodeMirror = null;
let chartCodeMirror = null;
document.getElementById('codeEditorTab').addEventListener('shown.bs.tab', (event) => {
  if(filterCodeMirror === null) {
    filterCodeMirror = monaco.editor.create(document.getElementById('codeEditorContent-filterCodeMirror'), {
      language: 'javascript',
      theme: 'vs-dark',
      insertSpaces: true,
      tabSize: 2
    });
  }
  if(transformCodeMirror === null) {
    transformCodeMirror = monaco.editor.create(document.getElementById('codeEditorContent-transformCodeMirror'), {
      language: 'javascript',
      theme: 'vs-dark',
      insertSpaces: true,
      tabSize: 2
    });
  }
  if(chartCodeMirror === null) {
    chartCodeMirror = monaco.editor.create(document.getElementById('codeEditorContent-chartCodeMirror'), {
      language: 'javascript',
      theme: 'vs-dark',
      insertSpaces: true,
      tabSize: 2
    });
  }
});

document.getElementById('codeEditorContent-downloadCodeButton').onclick = () => {
  chartCode.name = document.getElementById(`codeEditorContent-codeNameInput`).value;
  chartCode.filterCode = filterCodeMirror.getValue();
  chartCode.transformCode = transformCodeMirror.getValue();
  chartCode.chartCode = chartCodeMirror.getValue();
  fileDownload(JSON.stringify(chartCode, null, 2), `${utils.formatName(chartCode.name).substring(0, 100)}.json`);
};